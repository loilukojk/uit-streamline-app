package model

// Question - Question
type Question struct {
	tableName struct{} `pg:"question"`

	ID             int      `pg:"id,pk"`
	Content        string   `pg:"content"`
	Option         []string `pg:"option,array"`
	Answer         string   `pg:"answer"`
	LessonID       int      `pg:"lesson_id"`
	NumericalOrder int      `pg:"numerical_order"`
}
