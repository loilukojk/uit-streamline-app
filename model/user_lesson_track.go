package model

// UserLessonTrack - UserLessonTrack
type UserLessonTrack struct {
	tableName struct{} `pg:"user_lesson_track"`

	ID            int    `pg:"id,pk"`
	UserID        int    `pg:"userid"`
	LessonID      int    `pg:"lessonid"`
	LearnTimes    int    `pg:"learntimes"`
	LastTestDate  string `pg:"lasttestdate"`
	LastTestPoint int    `pg:"lasttestpoint"`
}
