package model

// User - User
type User struct {
	tableName struct{} `pg:"app_user"`

	ID       int    `pg:"id,pk"`
	Username string `pg:"username"`
	Password string `pg:"password"`
}
