package middleware

import (
	"github.com/labstack/echo"
)

//HandleResponse - Handle response for all service
func HandleResponse(c echo.Context, retCode int, isError bool, msg string, u interface{}) error {
	resp := new(Response)

	resp.Error = false
	resp.Message = msg
	resp.Data = u

	if isError {
		resp.Error = true
	}

	return c.JSON(retCode, resp)
}
