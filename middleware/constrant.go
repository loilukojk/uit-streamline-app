package middleware

type (
	//Response - Response struct
	Response struct {
		Error   bool        `json:"error"`
		Message string      `json:"msg"`
		Data    interface{} `json:"data"`
	}

	//ClientAnswer - ClientAnswer
	ClientAnswer struct {
		ClientAnswer []ContentClientAnswer `json:"clientanswer"`
	}

	// ContentClientAnswer - ContentClientAnswer
	ContentClientAnswer struct {
		QuestionID int    `json:"questionid"`
		Answer     string `json:"answer"`
	}

	//CheckAnswer - CheckAnswer
	CheckAnswer struct {
		CorrectedTotal     int                  `json:"correctedtotal"`
		CheckAnswerContent []CheckAnswerContent `json:"checkanswer"`
	}

	//CheckAnswerContent - CheckAnswerContent
	CheckAnswerContent struct {
		QuestionID int  `json:"questionid"`
		Correct    bool `json:"correct"`
	}
)
