# UIT - NT118.K13 - STREAMLINE APP

Phát triển ứng dụng trên thiết bị di động - Ứng dụng học tiếng Anh Streamline


## Description

- Người quản trị: Nguyễn Tấn Lợi.

- Ngôn ngữ lập trình: Golang.

- Liên hệ: Nguyễn Tấn Lợi (0386355353).

- Framework: Echo Framework


## How to Use

Dùng lệnh sau để chạy chương trình:
    go run main.go

Địa chỉ IP: localhost

Cổng kết nối: 1232


## How to Build

- Bước 1 - Cài đặt Database vào máy:
    + Tải và tiến hành cài đặt [PostgreSQL](https://www.postgresql.org/download/) vào máy.
        - PostgreSQL là 1 hệ quản trị cơ sở dữ liệu quan hệ và đối tượng.
        - **_Lưu ý_**: Kèm theo đó là tạo mật khẩu cho user "**_postgres_**" là "**_postgres123_**"
            + _user postgres sẽ dùng để kết nối đến database của app streamline_.
    + Tải và tiến hành cài đặt [pgAdmin](https://www.pgadmin.org/download/) vào máy (không cài cũng được).
        - pgAdmin là 1 công cụ hỗ trợ việc tương tác với PostgreSQL thông qua giao diện.
    + Tạo ra một Database mới trong PostgreSQL với thông tin như sau:
        - Database name: __STREAMLINE_APP__.
        - Owner: __postgres__.
    + Download file backup (*.bk) [streamline-data.bk](https://drive.google.com/drive/folders/1MIdhp6ilqrdpZkyy-281FCYQfW1SNqvA?usp=sharing)
        - Đây là file chứa các bảng dữ liệu, dữ liệu ban đầu của ứng dụng Streamline.
    + Tiến hành restore file backup vừa tải vào database "STREAMLINE_APP" mà bạn đã tạo trước đó.
    
- Bước 2 - Cài đặt [Golang](https://golang.org/dl/) vào máy.

- Bước 3 - Đặt project vào trong thư mục cấp dưới theo đường dẫn của biến GOPATH.

- Bước 4 - Cài đặt cái package liên quan:
    - Cú pháp: go get <tên package>
    - Các package cần cài đặt:        
        - github.com/go-pg/pg
	    - github.com/spf13/viper
        - github.com/labstack/echo
        - github.com/lib/pq        	           

- Bước 5 - Run project: go run main.go


## How to Config

Không cần thiết lập

## How to Test

Công cụ để test: [Postman](https://www.getpostman.com/downloads/)

- Chạy API sau để test: GET localhost:1232/lesson-content/111    
- Response: là một object JSON
    - "error": false,
    - "msg": "Get Lesson Content Success",
    - "data": {"ID": 111, "Name": "32", "Audio": "vendor/audios/connection/level2_32", "ImageContent": ["vendor/images/connection/level2_32a", "vendor/images/connection/level2_32b", "vendor/images/connection/level2_32c"], "LevelID": 2}
- Diễn giải: API trên phục vụ việc lấy nội dung của 1 bài học cụ thể.
        