package repository

import "github.com/uit-streamline-app/middleware"

// ILessonRepository - ILessonRepository
type ILessonRepository interface {
	GetLessonListByUserIDAndLevelID(int, int) (interface{}, error)
	GetLessonContentByID(int) (interface{}, error)
	GetQuestionListbyLessonID(int) (interface{}, error)
	CheckAnswerByLessonID(int, int, *middleware.ClientAnswer) (*middleware.CheckAnswer, error)
	UpdateUserTest(int, int, int) error
}
