package repository

import "github.com/uit-streamline-app/model"

// IUserRepository - IUserRepository
type IUserRepository interface {
	FindUser(*model.User) (interface{}, error)
	UserExist(*model.User) (bool, error)
	CreateNewUser(*model.User) error
	CreateUserLessonTrack(*model.User) error
	SetupLesson() error
}
