package service

import "github.com/labstack/echo"

// LessonService - LessonService
type LessonService interface {
	GetLessonListByUserIDAndLevelID(c echo.Context) error
	GetLessonContentByID(c echo.Context) error
	GetQuestionListbyLessonID(c echo.Context) error
	CheckAnswerByLessonID(c echo.Context) error
}
