package service

import (
	"log"
	"net/http"

	"github.com/labstack/echo"
	"github.com/uit-streamline-app/middleware"
	"github.com/uit-streamline-app/model"
	repo "github.com/uit-streamline-app/repository/interface"
	I "github.com/uit-streamline-app/service/interface"
)

type (
	// UserService - Handle with User Repository
	UserService struct {
		UserRepo repo.IUserRepository
	}
)

// NewUserService - NewUserService
func NewUserService(userRepo repo.IUserRepository) I.UserService {
	return &UserService{
		UserRepo: userRepo,
	}
}

// Login - User Login
func (h *UserService) Login(c echo.Context) error {
	// Bind request body to struct User
	u := new(model.User)
	if err := c.Bind(u); err != nil {
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "Login Failed", nil)
	}

	// Find User
	user, err := h.UserRepo.FindUser(u)
	if err != nil {
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "Login Failed", nil)
	}

	return middleware.HandleResponse(c, 200, false, "Login Success", user)
}

// SignUp - Sign Up
func (h *UserService) SignUp(c echo.Context) error {
	// Bind request body to struct User
	u := new(model.User)
	if err := c.Bind(u); err != nil {
		log.Println("Error while bind user. Detail:", err)
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "SignUp Failed", nil)
	}

	// Check User Exist
	exist, err := h.UserRepo.UserExist(u)
	if err != nil {
		log.Println(err)
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "SignUp Failed", nil)
	}

	if exist {
		log.Println("Can not sign up user. User Existed")
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "SignUp Failed, User existed", nil)
	}

	// Create New User
	if err := h.UserRepo.CreateNewUser(u); err != nil {
		log.Println("Error while create new user. Detail:", err)
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "SignUp Failed, can not create new user", nil)
	}

	// Create UserLessonTrack
	if err := h.UserRepo.CreateUserLessonTrack(u); err != nil {
		log.Println("Error while create user lesson track. Detail:", err)
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "SignUp Failed, can not create user lesson track", nil)
	}

	return middleware.HandleResponse(c, 200, false, "SignUp Success", nil)
}
